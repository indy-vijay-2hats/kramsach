
const colors = require('tailwindcss/colors')
module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: { 
    
    fontFamily: {
      'sans': ['"Roboto"'], 
     },
    
     container: {
        center: true, 
     },
    backgrounds:{
      white: '#FFFFFF',
      primary: '#262626',
      secondary: '#8b8b8b',  
      bggray:'#F2F2F2',
      graydrak:'#000000',
    }, 

    colors: { 
      transparent: 'transparent',
      current: 'currentColor',
      gray: colors.trueGray,
      red: colors.red,
      blue: colors.lightBlue,
      yellow: colors.amber, 
      white: '#FFFFFF',
      primary: '#262626',
      secondary: '#8b8b8b',  
      bggray:'#F2F2F2',
      graydrak:'#000000',
      txtgray :'#808080',
    },
    extend: { 
      letterSpacing: { 
      },
      maxWidth: { 
        '1050':'65.625rem',
        '620':'38.75rem',
        '535':'33.4375rem',
        '380':'23.75rem',
        '450':'28.125rem'
      },
      maxHeight: { 
        '600':'37.5rem',
        '380':'23.75rem'
      },
      lineHeight: { 
        'mainh1': '4.5rem', /*72*/
        'mainh2': '2rem', /*32*/
        'mainh3': '1.75rem', /*28*/  
        'para':'1.875rem',
        '28':'2.8rem'
      }, 
      padding: { 
        '66': '4.125rem',
        '50':'3.125rem',
        '15':'0.9375rem',
        '100':'6.25rem',
        '200':'12.5rem',
        '180':'11.25rem',
        '30':'1.875rem',
        '60':'3.75rem',
        '50':'3.125rem',
        '70':'4.375rem'
        
      },      
      margin: { 
        '1.5':'0.1875rem',
        '30':'1.875rem'
      },
       spacing: { 
      },
      width:{ 
        '354':'22.125rem',
        '535':'33.4375rem',
        '50':'3.125rem',
        '380':'23.75rem'
      },
      height: { 
        '22':'1.375rem',
        '1.5':'0.1875rem',
        '600':'37.5rem',
        '500':'31.25rem',
        '400':'25rem',
        '380':'23.75rem'
        
      },
      fontSize: { 
        'h1': '4.5rem', /*72*/
        'h2': '2rem', /*32*/
        'h3': '1.75rem', /*28*/
        'h4': '1.625rem', /*26*/
        'h5': '1.3125rem', /*21*/
        'h6': '1rem', /*16*/
        'p':'1.3125rem',/* 21 */
        '20':'1.25rem',
        '56':'3.5rem',
        '18':'1.125rem',
        '12':'0.75rem',
        '14':'0.875rem'
      },
      inset: {  
        '75':'4.6875rem',
        '50':'3.125rem',
      },
      zIndex: {
        '999': 999,
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
